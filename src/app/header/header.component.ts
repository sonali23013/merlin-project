import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name1 = '';

  constructor(private router: Router) {
  }

  ngOnInit() {
    const userdetails = JSON.parse(localStorage.getItem('userd'));
    this.name1 = userdetails.name;
    console.log(this.name1);
  }

  onLogout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  downloadPdf(){
    const link = document.createElement('a');
    link.download = 'filename';
    link.href = 'assets/file.pdf';
    link.click();
  }

}
