import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-modeling',
  templateUrl: './modeling.component.html',
  styleUrls: ['./modeling.component.css']
})
export class ModelingComponent implements OnInit {
name1 = '';
loginFlag = '';

  constructor(private router: Router) { }

  ngOnInit() {
    try {
      const userdetails = JSON.parse(localStorage.getItem('userd'));
      this.name1 = userdetails.name;
    } catch (e) {
      localStorage.clear();
      this.router.navigate(['/login']);
    }
      // console.log(this.name1);

      // this.loginFlag = localStorage.getItem('loginFlag');
      // // console.log(this.loginFlag);
      //
      // if (this.loginFlag === 'true') {
      //   console.log('Helllllloo');
      // } else {
      //   console.log('Faillll');
      // }
  }
}
