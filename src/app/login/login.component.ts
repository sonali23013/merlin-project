import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {createElementCssSelector} from '@angular/compiler';
import validate = WebAssembly.validate;
import {PlatformLocation} from '@angular/common';

@Component({
  selector: 'app-assingment-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
errorMsg = false;
successMsg = false;
// name = 'Sonali';
// role = 'admin';
username = '';
password = '';
errorMessage = '';
isValid = false;
reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
isLogin = false;
loginFlag = 'false';


  constructor(private router: Router) {

  }


  ngOnInit() {
    // console.log(this.router.url);
    // if (this.router.url === '/login'){
    //   console.log('Hello World');
    //   // this.isLogin = true;
    //   // this.loginFlag = 'true';
    //   // localStorage.setItem('loginFlag', this.loginFlag);
    // }
    if (localStorage.hasOwnProperty('userd')) {
      this.router.navigate(['/site-level']);
    }    else {
      this.router.navigate(['login']);
    }
  }


  checkLogin() {
    if (this.username === '') {
      this.errorMsg = true;
      this.successMsg = false;
      this.errorMessage = 'You cannot leave username empty!';
    } else if (!this.reg.test(this.username)) {
      this.errorMsg = true;
      this.successMsg = false;
      this.errorMessage = 'Enter valid email id';
    } else if (this.password === '') {
      this.errorMsg = true;
      this.successMsg = false;
      this.errorMessage = 'You cannot leave password empty!';
    } else if ((this.username === 'sonali.joshi@zs.com') && (this.password === '1234')) {
      this.isValid = true;
      this.errorMsg = false;
      this.successMsg = true;
      setTimeout(() => {
      this.isValid = false;
      this.router.navigate(['/site-level']);
    }, 2000);
      //  this.errorMessage = 'Log In successful';
      // localStorage.setItem(this.username, 'sonali.joshi@zs.com');
      // localStorage.setItem(this.name, 'Sonali');
      // localStorage.setItem(this.role, 'admin');
      const userdetails = {
      username: this.username,
      role: 'admin',
      name: 'Sonali'
    };
      localStorage.setItem('userd', JSON.stringify(userdetails));
      // console.log(JSON.parse(localStorage.getItem('userdetails')));
      console.log(JSON.stringify(userdetails));
    } else {
      this.errorMsg = true;
      this.successMsg = false;
      this.errorMessage = 'Log In failed, invalid credentials!';
    }
  }

  // getColor(){
  //   return this.messageMsg === true ? 'aquamarine' : 'moccasin';
  // }
}
