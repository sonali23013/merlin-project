import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-site-level',
  templateUrl: './site-level.component.html',
  styleUrls: ['./site-level.component.css']
})
export class SiteLevelComponent implements OnInit {
name1 = '';
loadedPosts = [];
errorMessage = false;
isFetching = true;
errorMsg = '';

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    try {
      const userdetails = JSON.parse(localStorage.getItem('userd'));
      this.name1 = userdetails.name;
    } catch (e) {
      localStorage.clear();
      this.router.navigate(['/login']);
    }
      // console.log(this.name1);
    this.onCallButton();
  }

  private onCallButton() {
    this.isFetching = true;
    this.http.get('https://jsonplaceholder.typicode.com/todos')
      .pipe(
        map(responseData => {
        const postArray = [];
          // tslint:disable-next-line:forin
        for (const key in responseData) {
          if (responseData.hasOwnProperty(key)) {
            postArray.push(responseData[key]);
          } else {
            console.log('No data');
          }
        }
        return postArray;
      }))
      .subscribe (
        posts => {
          // console.log(posts);
          this.isFetching = false;
          this.loadedPosts = posts;

          for (const items of this.loadedPosts) {
          if ( items.completed === true) {
            items.completed = 'complete';
          } else {
            items.completed = 'in progress';
          }
        }
      }, error => {
          this.isFetching = false;
          this.errorMessage = true;
          this.errorMsg = 'API Failure';
        });

  }

  fetchPosts() {
    this.onCallButton();
  }
}
