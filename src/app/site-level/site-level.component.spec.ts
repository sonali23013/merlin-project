import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiteLevelComponent } from './site-level.component';

describe('SiteLevelComponent', () => {
  let component: SiteLevelComponent;
  let fixture: ComponentFixture<SiteLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
